﻿namespace Calculator.Application
{
    public class Parameter
    {
        public Parameter(string variable, double min, double max, double step)
        {
            Variable = variable;
            Min = min;
            Max = max;
            Step = step;
        }

        public string Variable { get; }
        public double Min { get; }
        public double Max { get; }
        public double Step { get; }
    }
}
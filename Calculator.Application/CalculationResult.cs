﻿namespace Calculator.Application
{
    public class CalculationResult
    {
        public string[] Names { get; set; }
        
        public double[][] Values { get; set; }
    }
}
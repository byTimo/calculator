﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Calculator.Application.Calculation.Representatives;

namespace Calculator.Application.Calculation.Searchers
{
    public static class ModificationSearcher
    {
        private static readonly Regex modifiersPattern = new Regex(@"sin|cos|tg|sqrt|trpt|round|floor", RegexOptions.Compiled | RegexOptions.RightToLeft);

        private static readonly IReadOnlyDictionary<string, Modification> modifiers = new[]
        {
            new Modification("sin", (sequnces, previous) => Math.Sin(previous.Invoke(sequnces))),
            new Modification("cos", (sequnces, previous) => Math.Cos(previous.Invoke(sequnces))),
            new Modification("tg", (sequnces, previous) => Math.Tan(previous.Invoke(sequnces))),
            new Modification("sqrt", (sequnces, previous) => Math.Sqrt(previous.Invoke(sequnces))),
            new Modification("trpt", (sequnces, previous) => Math.Pow(previous.Invoke(sequnces), 1D / 3)),
            new Modification("round", (sequnces, previous) => Math.Round(previous.Invoke(sequnces))),
            new Modification("floor", (sequnces, previous) => Math.Floor(previous.Invoke(sequnces)))
        }.ToDictionary(x => x.Pattern);

        public static Modification Search(string expression, int startIndex, int length)
        {
            var match = modifiersPattern.Match(expression, startIndex > 0 ? startIndex : 0, length);
            return match.Success ? modifiers[match.Value] : null;
        }
    }
}
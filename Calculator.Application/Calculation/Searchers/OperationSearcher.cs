﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Calculator.Application.Calculation.Representatives;

namespace Calculator.Application.Calculation.Searchers
{
    public static class OperationSearcher
    {
        private static readonly Regex operationPattern = new Regex(@"[\+\-\^\*\/\%\)]", RegexOptions.Compiled | RegexOptions.RightToLeft);

        private static readonly IReadOnlyDictionary<string, Operation> operations = new Dictionary<string, Operation>
        {
            {"+", new Operation((x, f, s) => f.Invoke(x) + s.Invoke(x), 1)},
            {"-", new Operation((x, f, s) => f.Invoke(x) - s.Invoke(x), 1)},
            {"*", new Operation((x, f, s) => f.Invoke(x) * s.Invoke(x), 2)},
            {"/", new Operation((x, f, s) => f.Invoke(x) / s.Invoke(x), 2)},
            {"%", new Operation((x, f, s) => f.Invoke(x) % s.Invoke(x), 2)},
            {"^", new Operation((x, f, s) => Math.Pow(f.Invoke(x), s.Invoke(x)), 3)}
        };

        public static OperationSearchResult Search(string expression, int startIndex, int length)
        {
            var match = operationPattern.Match(expression, startIndex, length);
            return match.Success
                       ? new OperationSearchResult(match.Value, operations.TryGetValue(match.Value, out var operation) ? operation : null, match.Index)
                       : new OperationSearchResult("", null, -1);
        }
    }
}
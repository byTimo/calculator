﻿using System;
using System.Text.RegularExpressions;

namespace Calculator.Application.Calculation.Searchers
{
    public static class BracketSearcher
    {
        private static readonly Regex bracketPattern = new Regex(@"[\(\)]", RegexOptions.Compiled | RegexOptions.RightToLeft);

        public static SearchBracketResult Search(string expression, int startIndex, int lenght)
        {
            var closeBracketsCount = 0;
            var closeBracketIndex = 0;
            var match = bracketPattern.Match(expression, startIndex, lenght);

            while (match.Success)
            {
                switch (match.Value)
                {
                    case "(" when --closeBracketsCount == 0:
                        return new SearchBracketResult(match.Index, closeBracketIndex);
                    case ")" when closeBracketsCount == 0:
                        closeBracketIndex = match.Index;
                        closeBracketsCount++;
                        break;
                    case ")":
                        closeBracketsCount++;
                        break;
                }

                match = match.NextMatch();
            }
            throw new ArgumentException("Не нашли открывающую скобку");
        }
    }
}
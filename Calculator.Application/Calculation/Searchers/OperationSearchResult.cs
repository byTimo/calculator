﻿using System;
using System.Collections.Generic;
using Calculator.Application.Calculation.Representatives;

namespace Calculator.Application.Calculation.Searchers
{
    public class OperationSearchResult
    {
        public OperationSearchResult(string value, Operation operation, int index)
        {
            Value = value;
            Operation = operation?.Func;
            Weight = operation?.Weight ?? -1;
            Index = index;
        }

        public string Value { get; }
        public Func<IDictionary<string, double>, Func<IDictionary<string, double>, double>, Func<IDictionary<string, double>, double>, double> Operation { get; }
        public int Weight { get; }
        public int Index { get; }

        public override string ToString()
        {
            return Value;
        }
    }
}
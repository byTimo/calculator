﻿namespace Calculator.Application.Calculation.Searchers
{
    public class SearchBracketResult
    {
        public SearchBracketResult(int startIndex, int endIndex)
        {
            StartIndex = startIndex;
            EndIndex = endIndex;
        }

        public int StartIndex { get; }
        public int EndIndex { get; }
    }
}
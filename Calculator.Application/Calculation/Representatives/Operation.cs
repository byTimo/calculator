﻿using System;
using System.Collections.Generic;

namespace Calculator.Application.Calculation.Representatives
{
    public class Operation
    {
        public Operation(Func<IDictionary<string, double>, Func<IDictionary<string, double>, double>, Func<IDictionary<string, double>, double>, double> func, int weight)
        {
            Func = func;
            Weight = weight;
        }

        public Func<IDictionary<string, double>, Func<IDictionary<string, double>, double>, Func<IDictionary<string, double>, double>, double> Func { get; }
        public int Weight { get; }
    }
}
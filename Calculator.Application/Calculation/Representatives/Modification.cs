﻿using System;
using System.Collections.Generic;

namespace Calculator.Application.Calculation.Representatives
{
    public class Modification
    {
        public Modification(string pattern, Func<IDictionary<string, double>, Func<IDictionary<string, double>, double>, double> func)
        {
            Pattern = pattern;
            Func = func;
        }

        public string Pattern { get; }
        public Func<IDictionary<string, double>, Func<IDictionary<string, double>, double>, double> Func { get; }

        public override string ToString()
        {
            return Pattern;
        }
    }
}
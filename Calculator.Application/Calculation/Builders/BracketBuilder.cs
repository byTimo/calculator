﻿using Calculator.Application.Calculation.Searchers;

namespace Calculator.Application.Calculation.Builders
{
    public static class BracketBuilder
    {
        public static BuildingResult Build(string expression, int startIndex, int length)
        {
            var result = BracketSearcher.Search(expression, startIndex, length);
            var buildingResult = OperationBuilder.Build(expression, result.StartIndex + 1, result.EndIndex - result.StartIndex - 1);
            return ModificationBuilder.Build(new BuildingResult(buildingResult.Operation, result.StartIndex - 1), expression);
        }
    }
}
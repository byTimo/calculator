﻿using System.Text.RegularExpressions;

namespace Calculator.Application.Calculation.Builders
{
    public static class ValueBuilder
    {
        private static readonly Regex digitRegex = new Regex(@"\d+", RegexOptions.Compiled);

        public static BuildingResult Build(string expression, int startIndex, int length)
        {
            var targetExpression = expression.Substring(startIndex, length).Replace(',', '.');
            if (digitRegex.IsMatch(targetExpression))
            {
                var value = double.Parse(targetExpression);
                return new BuildingResult(sequences => value, startIndex);
            }
            return new BuildingResult(sequences => sequences[targetExpression], startIndex);
        }
    }
}
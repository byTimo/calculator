﻿using System;
using System.Collections.Generic;
using Calculator.Application.Calculation.Searchers;

namespace Calculator.Application.Calculation.Builders
{
    public static class OperationBuilder
    {
        public static BuildingResult Build(string expression, int startIndex, int length, Func<IDictionary<string, double>, double> previous = null)
        {
            if (length == 0)
            {
                return new BuildingResult(previous, -1);
            }

            var currentOperation = OperationSearcher.Search(expression, startIndex, length);
            if (currentOperation.Index == -1)
            {
                return ValueBuilder.Build(expression, startIndex, length);
            }

            if (currentOperation.Value.Equals(")"))
            {
                var bracketBuildingResult = BracketBuilder.Build(expression, startIndex, currentOperation.Index + 1 - startIndex);
                return Build(expression, startIndex, bracketBuildingResult.Next + 1 - startIndex, bracketBuildingResult.Operation);
            }

            previous = previous ?? ValueBuilder.Build(expression, currentOperation.Index + 1, length - currentOperation.Index - 1 + startIndex).Operation;

            var nextOperation = OperationSearcher.Search(expression, startIndex, currentOperation.Index - startIndex);

            if (nextOperation.Value.Equals(")"))
            {
                var buildingResult = BracketBuilder.Build(expression, startIndex, nextOperation.Index + 1 - startIndex);
                return Build(expression, startIndex, buildingResult.Next + 1 - startIndex, CreateNode(currentOperation.Operation, buildingResult.Operation, previous));
            }

            if (nextOperation.Index == -1 || currentOperation.Weight < nextOperation.Weight)
            {
                var dependencyOperation = Build(expression, startIndex, currentOperation.Index - startIndex);
                return new BuildingResult(CreateNode(currentOperation.Operation, dependencyOperation.Operation, previous), dependencyOperation.Next);
            }

            var valueNode = ValueBuilder.Build(expression, nextOperation.Index + 1, currentOperation.Index - nextOperation.Index - 1);
            var currentOperationNode = CreateNode(currentOperation.Operation, valueNode.Operation, previous);
            return Build(expression, startIndex, currentOperation.Index - startIndex, currentOperationNode);
        }

        private static Func<IDictionary<string, double>, double> CreateNode(
            Func<IDictionary<string, double>, Func<IDictionary<string, double>, double>, Func<IDictionary<string, double>, double>, double> operation,
            Func<IDictionary<string, double>, double> left,
            Func<IDictionary<string, double>, double> right)
        {
            return sequences => operation.Invoke(sequences, left, right);
        }
    }
}
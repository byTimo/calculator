﻿using System;
using System.Collections.Generic;

namespace Calculator.Application.Calculation.Builders
{
    public class BuildingResult
    {
        public BuildingResult(Func<IDictionary<string, double>, double> operation, int next)
        {
            Operation = operation;
            Next = next;
        }

        public Func<IDictionary<string, double>, double> Operation { get; }
        public int Next { get; }
    }
}
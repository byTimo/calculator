﻿using Calculator.Application.Calculation.Searchers;

namespace Calculator.Application.Calculation.Builders
{
    public static class ModificationBuilder
    {
        public static BuildingResult Build(BuildingResult operationBuildingResult, string exression)
        {
            var modification = ModificationSearcher.Search(exression, operationBuildingResult.Next - 4, 5);
            return modification == null
                       ? operationBuildingResult
                       : new BuildingResult(sequences => modification.Func.Invoke(sequences, operationBuildingResult.Operation), operationBuildingResult.Next - modification.Pattern.Length);
        }
    }
}
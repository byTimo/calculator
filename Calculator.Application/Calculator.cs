﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Calculator.Application.Sequences;

namespace Calculator.Application
{
    public static class Calculator 
    {
        public static async Task<CalculationResult> CalculateAsync(string expression, Parameter[] parameters, CancellationToken cancellation = default (CancellationToken))
        {
            var analysisTask = Task.Run(() => CalculationTreeBuilder.Build(expression), cancellation);
            var parametersPrepearTask = Task.Run(() => SequenceBuilder.Build(parameters.Select(x => Range(x.Min, x.Max, x.Step).ToArray()).ToArray()), cancellation);
            var names = parameters.Select(x => x.Variable).ToArray();
            await Task.WhenAll(analysisTask, parametersPrepearTask).ConfigureAwait(false);

            var sequences = parametersPrepearTask.Result;
            var calculationTree = analysisTask.Result;

            if (sequences.Length == 0)
                return new CalculationResult
                {
                    Names = new[] {"f"},
                    Values = new[] {new[] {calculationTree.Invoke(new Dictionary<string, double>())}}
                };

            var results = new List<double[]>(sequences.First().Length);
            
            for (var i = 0; i < sequences.First().Length; i++)
            {
                var stepParameters = names.Select((x, j) => Tuple.Create(x, sequences[j][i])).ToDictionary(x => x.Item1, x => x.Item2);
                var expressionsValue = calculationTree.Invoke(stepParameters);
                results.Add(new[] {expressionsValue}.Concat(stepParameters.Values).ToArray());
            }

            return new CalculationResult
            {
                Names = new[] {$"f({string.Join(",", names)})"}.Concat(names).ToArray(),
                Values = results.ToArray()
            };
        }

        private static IEnumerable<double> Range(double min, double max, double step)
        {
            for (var i = min; i <= max; i += step)
                yield return i;
        }
    }
}
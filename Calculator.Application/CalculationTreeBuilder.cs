﻿using System;
using System.Collections.Generic;
using Calculator.Application.Calculation.Builders;

namespace Calculator.Application
{
    public static class CalculationTreeBuilder
    {
        public static Func<IDictionary<string, double>, double> Build(string expression)
        {
            if (string.IsNullOrWhiteSpace(expression))
            {
                throw new ArgumentNullException(nameof(expression));
            }

            return OperationBuilder.Build(expression, 0, expression.Length).Operation;
        }
    }
}
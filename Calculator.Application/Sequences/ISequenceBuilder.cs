﻿namespace Calculator.Application.Sequences
{
    public interface ISequenceBuilder
    {
        double[][] Build(double[][] ranges);
    }
}
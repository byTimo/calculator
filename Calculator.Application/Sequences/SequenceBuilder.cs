﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Calculator.Application.Sequences
{
    public static class SequenceBuilder
    {
        public static double[][] Build(double[][] ranges)
        {
            var multipliers = CalculateReapetMultipliers(ranges);

            return ranges.Select((x, i) => CreateSequence(x, multipliers[i].Item1, multipliers[i].Item2)).ToArray();
        }

        private static double[] CreateSequence(IEnumerable<double> values, int valueRepeater, int sequenceRepeater)
        {
            return Enumerable.Repeat(values.SelectMany(y => Enumerable.Repeat(y, valueRepeater)), sequenceRepeater).SelectMany(x => x).ToArray();
        }

        private static Tuple<int, int>[] CalculateReapetMultipliers(double[][] ranges)
        {
            return ranges.Select((x, i) => Tuple.Create(
                                                        i == ranges.Length - 1 ? 1 : ranges.Skip(i + 1).Aggregate(1, (multiplier, range) => multiplier * range.Length),
                                                        i == 0 ? 1 : ranges.Take(i).Aggregate(1, (multiplier, range) => multiplier * range.Length)
                                                       ))
                         .ToArray();
        }
    }
}
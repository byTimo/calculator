using System.Web.Configuration;

namespace Calculator.Web
{
    public static class AppSettings
    {
        public static string MongoHost { get; } = WebConfigurationManager.AppSettings["MongoHost"];
        public static string MongoUserName { get; } = WebConfigurationManager.AppSettings["MongoUserName"];
        public static string MongoPassword { get; } = WebConfigurationManager.AppSettings["MongoPassword"];
        public static string MongoDbName { get; } = WebConfigurationManager.AppSettings["MongoDbName"];
    }
}
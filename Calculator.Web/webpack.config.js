const webpack = require('webpack');
const path = require('path');

module.exports = {
    resolve: {
        extensions: ['*', '.js', '.tsx', '.ts']
    },
    devtool: 'inline-source-map',
    entry: [
        path.resolve(__dirname, 'src/index.tsx'),
        path.resolve(__dirname, 'src/constantsModuleImports.js')
    ],
    target: 'web',
    output: {
        path: path.resolve(__dirname, 'dist/'),
        publicPath: '/',
        filename: 'bundle.js'
    },
    plugins: [
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': JSON.stringify('development'),
            __DEV__: true
        }),
        new webpack.NoEmitOnErrorsPlugin(),
    ],
    module: {
        rules: [
            {test: /\.tsx?$/, loader: "ts-loader"},
            {test: /\.eot(\?v=\d+.\d+.\d+)?$/, loader: 'file-loader'},
            {test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: 'url-loader?limit=10000&mimetype=application/font-woff'},
            {test: /\.[ot]tf(\?v=\d+.\d+.\d+)?$/, loader: 'url-loader?limit=10000&mimetype=application/octet-stream'},
            {test: /\.svg(\?v=\d+\.\d+\.\d+)?$/, loader: 'url-loader?limit=10000&mimetype=image/svg+xml'},
            {test: /\.(jpe?g|png|gif)$/i, loader: 'file-loader?name=[name].[ext]'},
            {test: /\.ico$/, loader: 'file-loader?name=[name].[ext]'},
            {test: /\.css$/, loaders: ['style-loader', 'css-loader?sourceMap']}
        ]
    }
};

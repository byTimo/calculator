﻿using System;

namespace Calculator.Web.Dal
{
    public class CalculationResultsEntity
    {
        public Guid Id { get; set; } = Guid.NewGuid();

        public string Formula { get; set; }

        public string[] Variables { get; set; }

        public double[][] Values { get; set; }
    }
}
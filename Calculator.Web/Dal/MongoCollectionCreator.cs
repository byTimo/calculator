﻿using System.Collections.Generic;
using System.Security.Authentication;
using MongoDB.Bson;
using MongoDB.Driver;

namespace Calculator.Web.Dal
{
    public static class MongoCollectionCreator
    {
        private static IMongoClient client;

        private static IMongoClient Client => client ?? (client = CreateClient());

        public static IMongoCollection<TEntity> Create<TEntity>(string collectionName)
        {
            return Client
                .GetDatabase(AppSettings.MongoDbName, new MongoDatabaseSettings {GuidRepresentation = GuidRepresentation.CSharpLegacy})
                .GetCollection<TEntity>(collectionName, new MongoCollectionSettings {GuidRepresentation = GuidRepresentation.CSharpLegacy});
        }

        private static IMongoClient CreateClient()
        {
            return new MongoClient(new MongoClientSettings
            {
                Server = new MongoServerAddress(AppSettings.MongoHost, 10255),
                UseSsl = true,
                SslSettings = new SslSettings {EnabledSslProtocols = SslProtocols.Tls12},
                Credentials = new List<MongoCredential>
                {
                    new MongoCredential(
                                        "SCRAM-SHA-1",
                                        new MongoInternalIdentity(AppSettings.MongoDbName, AppSettings.MongoUserName),
                                        new PasswordEvidence(AppSettings.MongoPassword)
                                       )
                }
            });
        }
    }
}
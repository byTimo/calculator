﻿using System.Threading.Tasks;
using System.Xml;
using Microsoft.Azure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.File;

namespace Calculator.Web.Dal
{
    public static class FormulaRepository
    {
        private static CloudStorageAccount storageAccount;

        private static CloudStorageAccount StorageAccount => storageAccount ?? (storageAccount = CloudStorageAccount.Parse(CloudConfigurationManager.GetSetting("StorageConnectionString")));

        private static CloudFileDirectory RootDirectory => StorageAccount.CreateCloudFileClient().GetShareReference("formula").GetRootDirectoryReference();
        
        private static CloudFile FormulaFile => RootDirectory.GetFileReference("Formula.xml");
        
        public static async Task<string> Read()
        {
            var xml = await FormulaFile.DownloadTextAsync().ConfigureAwait(false);
            var formulaDoc = new XmlDocument();
            formulaDoc.LoadXml(xml);
            return formulaDoc["formula"]?.InnerText;
        }

        public static async Task Save(string formula)
        {
            var xml = await FormulaFile.DownloadTextAsync().ConfigureAwait(false);
            var formulaDoc = new XmlDocument();
            formulaDoc.LoadXml(xml);
            formulaDoc["formula"].InnerText = formula;
            await FormulaFile.UploadTextAsync(formulaDoc.InnerXml).ConfigureAwait(false);
        }
    }
}
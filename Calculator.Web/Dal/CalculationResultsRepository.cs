﻿using System.Threading;
using System.Threading.Tasks;

namespace Calculator.Web.Dal
{
    public static class CalculationResultsRepository
    {
        private const string CollectionName = "CalculationResultsEntity";

        public static async Task SaveAsync(CalculationResultsEntity entity, CancellationToken cancellation)
        {
            var collection = MongoCollectionCreator.Create<CalculationResultsEntity>(CollectionName);
            await collection.InsertOneAsync(entity, null, cancellation).ConfigureAwait(false);
        }
    }
}
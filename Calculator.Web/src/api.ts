import {Promise} from "es6-promise";
import {CalculationResults, Variable} from "./store/rootState";

const formulaUrl: string = "api/formula";
const calculateUrl: string = "api/calculate";

export default class Api {
    static loadFormula(): Promise<{ ok: boolean, body?: string }> {
        return Api.get(formulaUrl);
    }

    static saveFormula(formula: string): Promise<{ ok: boolean }> {
        return Api.post(`${formulaUrl}?f=${encodeURIComponent(formula)}`);
    }

    static calculate(formula: string, variables: Variable[]): Promise<{ ok: boolean, body?: CalculationResults }> {
        return Api.post(calculateUrl, {
            formula,
            variables
        });
    }

    private static get<T>(url: string): Promise<{ ok: boolean, body?: T }> {
        return fetch(url, {
            method: "GET",
            mode: "same-origin"
        }).then((response: Response) => {
            if (!response.ok)
                throw new Error();
            if (response.status === 200)
                return response.json();
            return new Promise(resolve => resolve(null))
        }).then(x => {
            return {ok: true, body: x}
        }).catch(error => {
            return {ok: false}
        });
    }

    private static post<T>(url: string, body?: any): Promise<{ ok: boolean, body?: T }> {
        return fetch(url, {
            method: "POST",
            mode: "same-origin",
            headers: {"Content-Type": "application/json"},
            body: JSON.stringify(body)
        }).then((response: Response) => {
            if (!response.ok)
                throw new Error();
            if (response.status === 200)
                return response.json();
            return new Promise(resolve => resolve(null))
        }).then(x => {
            return {ok: true, body: x}
        }).catch(error => {
            return {ok: false}
        });

    }
}
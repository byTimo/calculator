import types from '../actions/actionTypes';
import {initialState} from "../store/rootState";

export default function (state = initialState.requestCount, action: { type: string }) {
    switch (action.type) {
        case types.request.BEGIN_API_REQUEST:
            return state + 1;
        case types.request.END_API_REQUEST:
            return state - 1;
        default:
            return state;
    }
}

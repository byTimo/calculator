import Types from '../actions/actionTypes';
import {CalculationResults, initialState} from "../store/rootState";

export default function (state = initialState.calculator, action: { type: string, payload?: any }) {
    switch (action.type) {
        case Types.formula.GET_FORMULA_SUCCESS:
        case Types.formula.CHANGE_FORMULA:
            return {...state, formula: action.payload};
        case Types.variables.ADD_VARIABLE:
            let variables = [...state.variables, {name: "", min: 0, max: 0, step: 0}];
            return {...state, variables: variables};
        case Types.variables.REMOVE_VARIABLE:
            const index: number = action.payload;
            variables = [... state.variables.filter((x, i: number) => i !== index)];
            return {...state, variables: variables};
        case Types.variables.CHANGE_VARIABLE:
            const change: { index: number, field: string, value: string | number } = action.payload;
            variables = [...state.variables];
            const targetVariable = {... variables[change.index]};
            targetVariable[change.field] = change.value;
            variables[change.index] = targetVariable;
            return {...state, variables: variables};
        case Types.formula.CALCULATE_FORMULA_SUCCESS:
            const results: CalculationResults = action.payload;
            return {...state, results: results};
        default:
            return state;
    }
}

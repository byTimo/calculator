import types from '../actions/ActionTypes';
import {initialState} from "../store/rootState";

export default function (state = initialState.notification, action: { type: string, payload?: any }) {
    switch (action.type) {
        case types.notification.SHOW_NOTIFICATION:
            return {text: action.payload, opened: true};
        case types.notification.HIDE_NOTIFICATION:
            return {text: "", opened: false};
        default:
            return state;
    }
}

import * as Redux from 'redux';
import RootState from "../store/rootState";
import {routerReducer} from "react-router-redux";
import notificationReducer from "./notificationReducer";
import requestReducer from "./requestReducer";
import calculatorReducer from "./calculatorReducer";

export default Redux.combineReducers<RootState>({
    calculator: calculatorReducer,
    notification: notificationReducer,
    requestCount: requestReducer,
    router: routerReducer
});

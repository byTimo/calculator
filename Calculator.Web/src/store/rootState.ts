export default interface RootState {
    calculator: {
        formula: string,
        errors: any
        variables: Variable[]
        results: CalculationResults
    }
    
    notification: {
        text: string,
        opened: boolean
    },
    requestCount: number
}

export interface Variable {
    name: string,
    min: number,
    max: number,
    step: number
}

export interface CalculationResults {
    names: string[],
    values: number[][],
}

export const initialState: RootState = {
    calculator: {
        formula: "",
        errors: {},
        variables: [],
        results: null
    },
    notification: {
        text: "",
        opened: false
    },
    requestCount: 0
};
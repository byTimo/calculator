import * as Redux from 'redux';
import rootReducer from '../reducers/index';
import thunk from 'redux-thunk';
import RootState, {initialState} from "./rootState";
import {routerMiddleware} from 'react-router-redux'
import createHistory from 'history/createBrowserHistory';
import immutableStateMiddleware from 'redux-immutable-state-invariant';
import {History} from "history";

export const history: History = createHistory();

export default (state: RootState = initialState): Redux.Store<RootState> => {
    const immutableStateMiddlewareInstance = immutableStateMiddleware();
    const routerMiddlewareInstance = routerMiddleware(history);
    const middleware = Redux.applyMiddleware(immutableStateMiddlewareInstance, thunk, routerMiddlewareInstance);
    return Redux.createStore<RootState>(rootReducer, state, middleware);
}

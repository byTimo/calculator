import * as React from 'react';
import * as ReactDOM from 'react-dom';
import createStore, {history} from './store/createStore';
import {Store} from "redux";
import IRootState from "./store/rootState";
import Root from "./app/Root";
import {loadFormula} from "./actions/calculationActions";
import injectTapEventPlugin = require("react-tap-event-plugin");

injectTapEventPlugin();

const store: Store<IRootState> = createStore();

store.dispatch(loadFormula());

ReactDOM.render(
    <Root store={store} history={history}/>,
    document.getElementById("app")
);

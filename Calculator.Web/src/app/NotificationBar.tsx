import * as React from 'react';
import * as actions from '../actions/NotificationActions';
import {Snackbar} from "material-ui";
import RootState from "../store/rootState";
import {bindActionCreators, Dispatch} from "redux";
import {connect} from "react-redux";

export interface NotificationsBarProps {
    isOpened: boolean,
    text: string,
    actions: any
}

export interface NotificationsBarState {
    isOpened: boolean,
    text: string
}

class NotificationsBar extends React.Component<NotificationsBarProps, NotificationsBarState> {
    constructor(props: NotificationsBarProps) {
        super(props);
        this.closeNotification = this.closeNotification.bind(this);

        this.state = {isOpened: props.isOpened, text: props.text};
    }

    componentWillReceiveProps(nextProps: NotificationsBarProps) {
        this.setState({isOpened: nextProps.isOpened, text: nextProps.text})
    }

    closeNotification() {
        this.props.actions.closeNotification();
    }

    render() {
        return (
            <Snackbar
                message={this.state.text}
                open={this.state.isOpened}
                autoHideDuration={3000}
                onRequestClose={this.closeNotification}
            />
        )
    }
}

function mapStateToProps(state: RootState) {
    return {
        isOpened: state.notification.opened,
        text: state.notification.text
    }
}

function mapDispatchToProps(dispatch: Dispatch<RootState>) {
    return {
        actions: bindActionCreators<any>(actions, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(NotificationsBar);

import * as React from 'react';
import {LinearProgress} from "material-ui";
import RootState from "../store/rootState";
import {connect} from "react-redux";

export interface RequestBarProps {
    isShowed: boolean
}

export interface RequestBarState {
    isShowed: boolean
}

class RequestBar extends React.Component<RequestBarProps, RequestBarState> {
    constructor(props: RequestBarProps) {
        super(props);
        this.state = {
            isShowed: props.isShowed
        }
    }

    componentWillReceiveProps(nextProps: RequestBarProps) {
        this.setState({isShowed: nextProps.isShowed});
    }

    render() {
        return this.state.isShowed ? <LinearProgress mode="indeterminate"/> : null
    }
}

function mapStateToProps(state: RootState) {
    return {
        isShowed: state.requestCount > 0
    }
}

export default connect(mapStateToProps)(RequestBar);

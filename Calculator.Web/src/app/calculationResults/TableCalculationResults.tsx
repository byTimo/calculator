import * as React from 'react';
import {CalculationResults} from "../../store/rootState";
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from "material-ui";

export interface TableCalculationResultsProps {
    results: CalculationResults
}

const TableCalculationResults: React.SFC<TableCalculationResultsProps> = (props: TableCalculationResultsProps) => {
    if (props.results == null)
        return null;
    return (
        <Table
            fixedHeader={true}
        >
            <TableHeader
                displaySelectAll={false}
                adjustForCheckbox={false}
            >
                <TableRow>
                    {props.results.names.map(x =>
                        <TableHeaderColumn key={x}>{x}</TableHeaderColumn>
                    )}
                </TableRow>
            </TableHeader>
            <TableBody
                displayRowCheckbox={false}
                stripedRows={true}
            >
                {props.results.values.map((x, i) =>
                    <TableRow key={i}>
                        {x.map(y =>
                            <TableRowColumn key={y.toString()}>{y}</TableRowColumn>
                        )}
                    </TableRow>
                )}
            </TableBody>
        </Table>
    )
};

export default TableCalculationResults;
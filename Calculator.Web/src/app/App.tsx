import * as React from "react";
import {MuiThemeProvider} from "material-ui/styles";
import NotificationBar from './NotificationBar';
import CalculatorPage from './calculator/CalculatorPage';
import ProgressBar from './ProgressBar';

export interface AppProps {
}

export default class App extends React.Component<AppProps, {}> {
    render() {
        return (
            <MuiThemeProvider>
                <div>
                    <ProgressBar/>
                    <CalculatorPage/>
                    <NotificationBar/>
                </div>
            </MuiThemeProvider>
        )
    }
}

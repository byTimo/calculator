import * as React from 'react';
import {IconButton, TextField} from "material-ui";
import Save from "material-ui/svg-icons/content/save"

export interface FormulaFormProps {
    formula: string,
    disabled: boolean,
    error?: string,
    onChange: (event: React.FormEvent<HTMLInputElement>) => void,
    onSave: () => void
}

const FormulaForm: React.SFC<FormulaFormProps> = (props: FormulaFormProps) => {
    return (
        <div>
            <TextField
                value={props.formula}
                name="formula"
                floatingLabelText="Редактирование формулы"
                errorText={props.error}
                disabled={props.disabled}
                onChange={props.onChange}
            />
            <IconButton
                disabled={props.disabled}
                tooltip="Схоранить эту формулу"
                onClick={props.onSave}
            >
                <Save/>
            </IconButton>
        </div>
    )
};

export default FormulaForm;
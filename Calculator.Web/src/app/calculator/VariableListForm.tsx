import * as React from 'react';
import {Variable} from "../../store/rootState";
import {IconButton, RaisedButton, TextField} from "material-ui";
import RemoveIcon from 'material-ui/svg-icons/content/remove'

export interface VariableListFormProps {
    variables: Variable[],
    errors: any
    disabled: boolean
    onAdd: () => void,
    onChange: (index: number, event: React.FormEvent<HTMLInputElement>) => void;
    onRemove: (index: number) => void;
}

const VariableListForm: React.SFC<VariableListFormProps> = (props: VariableListFormProps) => {
    const errors = props.errors || {};
    return (
        <div>
            {props.variables.map((variable, i: number) => {
                const error = errors[variable.name] || {};
                return (
                    <div key={i}>
                        <TextField
                            name="name"
                            value={variable.name}
                            floatingLabelText="Название"
                            disabled={props.disabled}
                            errorText={error["name"]}
                            onChange={(event: React.FormEvent<HTMLInputElement>) => props.onChange(i, event)}
                        />
                        <TextField
                            name="min"
                            value={variable.min}
                            floatingLabelText="Минимальное значение"
                            disabled={props.disabled}
                            errorText={error["min"]}
                            onChange={(event: React.FormEvent<HTMLInputElement>) => props.onChange(i, event)}
                        />
                        <TextField
                            name="max"
                            value={variable.max}
                            floatingLabelText="Максимальное значение"
                            disabled={props.disabled}
                            errorText={error["max"]}
                            onChange={(event: React.FormEvent<HTMLInputElement>) => props.onChange(i, event)}
                        />
                        <TextField
                            name="step"
                            value={variable.step}
                            floatingLabelText="Шаг"
                            disabled={props.disabled}
                            errorText={error["step"]}
                            onChange={(event: React.FormEvent<HTMLInputElement>) => props.onChange(i, event)}
                        />
                        <IconButton
                            tooltip="Удалить"
                            disabled={props.disabled}
                            onClick={() => props.onRemove(i)}
                        >
                            <RemoveIcon/>
                        </IconButton>
                    </div>
                )
            })}
            <RaisedButton
                label="Добавить переменную"
                primary={true}
                disabled={props.disabled}
                onClick={props.onAdd}
            />
        </div>
    );
};

export default VariableListForm;
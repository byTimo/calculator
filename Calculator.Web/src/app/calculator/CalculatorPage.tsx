import * as React from 'react';
import FormulaForm from "./FormulaForm";
import RootState, {CalculationResults, Variable} from "../../store/rootState";
import {connect} from "react-redux";
import VariableListForm from "./VariableListForm";
import {RaisedButton} from "material-ui";
import TableCalculationResults from "../calculationResults/TableCalculationResults";
import * as Redux from "redux";
import * as actions from '../../actions/calculationActions';

export interface CalculatorPageProps {
    formula: string,
    variables: Variable[],
    hasRequest: boolean,
    errors: any,
    results: CalculationResults,
    actions: any
}

export interface CalculatorPageState {
    formula: string,
    variables: Variable[],
    hasRequest: boolean,
    errors: any,
    results: CalculationResults
}

class CalculatorPage extends React.Component<CalculatorPageProps, CalculatorPageState> {
    constructor(props: CalculatorPageProps) {
        super(props);
        this.changedFormula = this.changedFormula.bind(this);
        this.saveFormula = this.saveFormula.bind(this);
        this.variableAdded = this.variableAdded.bind(this);
        this.variableChanged = this.variableChanged.bind(this);
        this.variableRemoved = this.variableRemoved.bind(this);
        this.calculate = this.calculate.bind(this);

        this.state = {
            formula: props.formula,
            variables: props.variables,
            hasRequest: props.hasRequest,
            errors: props.errors,
            results: props.results
        }
    }

    componentWillReceiveProps(nextProps: CalculatorPageProps) {
        this.setState({
            formula: nextProps.formula,
            variables: nextProps.variables,
            errors: nextProps.errors,
            hasRequest: nextProps.hasRequest,
            results: nextProps.results
        });
    }

    changedFormula(event: React.FormEvent<HTMLInputElement>) {
        event.preventDefault();
        const formula = event.currentTarget.value;
        this.props.actions.formulaChanged(formula);
    }

    saveFormula() {
        this.props.actions.saveFormula(this.state.formula);
    }

    variableAdded() {
        this.props.actions.variableAdded();
    }

    variableChanged(index: number, event: React.FormEvent<HTMLInputElement>) {
        this.props.actions.variableChanged(index, event.currentTarget.name, event.currentTarget.value);
    }

    variableRemoved(index: number) {
        this.props.actions.variableRemoved(index);
    }

    calculate() {
        this.props.actions.calculate(this.state.formula, this.state.variables);
    }

    render() {
        return (
            <div>
                <FormulaForm
                    formula={this.state.formula}
                    disabled={this.state.hasRequest}
                    error={this.state.errors['formula']}
                    onChange={this.changedFormula}
                    onSave={this.saveFormula}
                />
                <VariableListForm
                    variables={this.state.variables}
                    errors={this.state.errors["variables"]}
                    disabled={this.state.hasRequest}
                    onAdd={this.variableAdded}
                    onChange={this.variableChanged}
                    onRemove={this.variableRemoved}
                />
                <RaisedButton label="Расcчитать" disabled={this.state.hasRequest} onClick={this.calculate}/>
                <TableCalculationResults results={this.state.results}/>
            </div>
        )
    }
}

function mapStateToProps(state: RootState) {
    return {
        formula: state.calculator.formula,
        variables: state.calculator.variables,
        errors: state.calculator.errors,
        hasRequest: state.requestCount > 0,
        results: state.calculator.results
    }
}

function mapDispatchToProps(dispatch: Redux.Dispatch<RootState>) {
    return {
        actions: Redux.bindActionCreators<any>(actions, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(CalculatorPage);
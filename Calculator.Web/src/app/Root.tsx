import * as React from "react";
import {Provider} from "react-redux";
import App from "./App";
import RootState from "../store/rootState";
import {Store} from "redux";
import {History} from "history";
import {ConnectedRouter} from "react-router-redux";

export interface RootProps {
    store: Store<RootState>,
    history: History
}

export default class Root extends React.Component<RootProps> {
    render() {
        return (
            <Provider store={this.props.store}>
                <ConnectedRouter history={this.props.history}>
                    <App/>
                </ConnectedRouter>
            </Provider>
        );
    }
}

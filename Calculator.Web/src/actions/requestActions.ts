import types from './actionTypes';

export function beginRequest() {
    return {type: types.request.BEGIN_API_REQUEST};
}

export function endRequest() {
    return {type: types.request.END_API_REQUEST};
}

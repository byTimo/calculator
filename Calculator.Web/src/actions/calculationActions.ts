import Types from './actionTypes';
import RootState, {CalculationResults, Variable} from "../store/rootState";
import * as Redux from "redux";
import {beginRequest, endRequest} from "./requestActions";
import Api from "../api";
import {showNotification} from "./notificationActions";

export function loadFormulaSuccess(formula: string) {
    return {type: Types.formula.GET_FORMULA_SUCCESS, payload: formula};
}

export function loadFormulaFail() {
    return {type: Types.formula.GET_FORMULA_FAIL};
}

export function formulaChanged(formula: string) {
    return {type: Types.formula.CHANGE_FORMULA, payload: formula}
}

export function variableAdded() {
    return {type: Types.variables.ADD_VARIABLE}
}

export function variableRemoved(index: number) {
    return {type: Types.variables.REMOVE_VARIABLE, payload: index}
}

export function variableChanged(index: number, field: string, value: string | number) {
    return {type: Types.variables.CHANGE_VARIABLE, payload: {index, field, value}}
}

export function calculateFormulaSuccess(results: CalculationResults) {
    return {type: Types.formula.CALCULATE_FORMULA_SUCCESS, payload: results}
}

export function calculateFormulaFail() {
    return {type: Types.formula.CALCULATE_FORMULA_FAIL,}
}

export function loadFormula() {
    return (dispatch: Redux.Dispatch<RootState>) => {
        dispatch(beginRequest());
        return Api.loadFormula().then(result => {
            dispatch(endRequest());
            if (result.ok) {
                dispatch(showNotification("Успешно загрузили формулу"));
                dispatch(loadFormulaSuccess(result.body));
            } else {
                dispatch(showNotification("На сервере произошла ошибка"));
                dispatch(loadFormulaFail())
            }
        })
    }
}

export function saveFormula(formula: string) {
    return (dispatch: Redux.Dispatch<RootState>) => {
        dispatch(beginRequest());
        return Api.saveFormula(formula).then(result => {
            dispatch(endRequest());
            if (result.ok) {
                dispatch(showNotification("Успешно сохранили формулу"));
            } else {
                dispatch(showNotification("На сервере произошла ошибка"));
            }
        })
    }

}

export function calculate(formula: string, variables: Variable[]) {
    return (dispatch: Redux.Dispatch<RootState>) => {
        dispatch(beginRequest());
        return Api.calculate(formula, variables).then(result => {
            dispatch(endRequest());
            if (result.ok) {
                dispatch(showNotification("Успешно рассчитачли значение формулы"));
                dispatch(calculateFormulaSuccess(result.body));
            } else {
                dispatch(showNotification("На сервере произошла ошибка"));
                dispatch(calculateFormulaFail())
            }
        })
    }
}
import types from './ActionTypes';

export function closeNotification() {
    return {type: types.notification.HIDE_NOTIFICATION};
}

export function showNotification(text: string) {
    return {type: types.notification.SHOW_NOTIFICATION, payload: text};
}

﻿namespace Calculator.Web.Controllers.Entity
{
    public class CalculationInfo
    {
        public string Formula { get; set; }
        public CalculationVariable[] Variables { get; set; }
    }
}
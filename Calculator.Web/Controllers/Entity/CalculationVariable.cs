﻿namespace Calculator.Web.Controllers.Entity
{
    public class CalculationVariable
    {
        public string Name { get; set; }
        public double Min { get; set; }
        public double Max { get; set; }
        public double Step { get; set; }
    }
}
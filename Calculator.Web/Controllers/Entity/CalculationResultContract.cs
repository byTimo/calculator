﻿using System.Runtime.Serialization;

namespace Calculator.Web.Controllers.Entity
{
    [DataContract]
    public class CalculationResultContract
    {
        [DataMember(Name = "names")]
        public string[] Names { get; set; }
        
        [DataMember(Name = "values")]
        public double[][] Values { get; set; }
    }
}
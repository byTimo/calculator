﻿using System.Threading.Tasks;
using System.Web.Http;
using Calculator.Web.Dal;

namespace Calculator.Web.Controllers
{
    public class FormulaController : ApiController
    {
        public async Task<string> Get()
        {
            return await FormulaRepository.Read().ConfigureAwait(false);
        }

        public async  Task Post([FromUri]string f)
        {
            await FormulaRepository.Save(f).ConfigureAwait(false);
        }
    }
}
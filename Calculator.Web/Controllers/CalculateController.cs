﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using Calculator.Application;
using Calculator.Web.Controllers.Entity;
using Calculator.Web.Dal;

namespace Calculator.Web.Controllers
{
    public class CalculateController : ApiController
    {
        public async Task<CalculationResultContract> Post(CalculationInfo info)
        {
            var result = await Application.Calculator.CalculateAsync(info.Formula, info.Variables.Select(x => new Parameter(x.Name, x.Min, x.Max, x.Step)).ToArray()).ConfigureAwait(false);
            await CalculationResultsRepository.SaveAsync(new CalculationResultsEntity
            {
                Formula = info.Formula,
                Variables = result.Names,
                Values = result.Values
            }, CancellationToken.None).ConfigureAwait(false);
            return new CalculationResultContract
            {
                Names = result.Names,
                Values = result.Values
            };
        }
    }
}
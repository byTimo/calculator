﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using NUnit.Framework;

namespace Calculator.Application.Tests
{
    [TestFixture]
    public class CalculatorTests
    {      
        [TestCaseSource(nameof(Calculate_Cases))]
        public async Task Calculate(string expression, Parameter[] parameters, CalculationResult expected)
        {
            var actual = await Calculator.CalculateAsync(expression, parameters);

            Assert.That(actual.Names, Is.EqualTo(expected.Names));
            Assert.That(actual.Values, Is.EquivalentTo(expected.Values));
        }

        private static IEnumerable<TestCaseData> Calculate_Cases()
        {
            yield return new TestCaseData(
                                          "1+1",
                                          Array.Empty<Parameter>(),
                                          new CalculationResult
                                          {
                                              Names = new[] {"f(x)"},
                                              Values = new[]
                                              {
                                                  new[] {2D},
                                              }
                                          })
                {TestName = "Calculate_WithoutParameters_ReturnOneValue"};


            yield return new TestCaseData(
                                          "x+1",
                                          new[] {new Parameter("x", 0, 10, 1)},
                                          new CalculationResult
                                          {
                                              Names = new[] {"f(x)", "x"},
                                              Values = new[]
                                              {
                                                  new[] {1D, 0},
                                                  new[] {2D, 1},
                                                  new[] {3D, 2},
                                                  new[] {4D, 3},
                                                  new[] {5D, 4},
                                                  new[] {6D, 5},
                                                  new[] {7D, 6},
                                                  new[] {8D, 7},
                                                  new[] {9D, 8},
                                                  new[] {10D, 9},
                                                  new[] {11D, 10},
                                                  
                                              }
                                          })
                {TestName = "Calculate_WithOneParameter_ReturnOneValue"};

            yield return new TestCaseData(
                                          "x+y",
                                          new[]
                                          {
                                              new Parameter("x", 3, 5, 2),
                                              new Parameter("y", -1, 1, 1),
                                          },
                                          new CalculationResult
                                          {
                                              Names = new[] {"f(x)", "x", "y"},
                                              Values = new[]
                                              {
                                                  new[] {2D, 3, -1},
                                                  new[] {3D, 3, 0},
                                                  new[] {4D, 3, 1},
                                                  new[] {4D, 5, -1},
                                                  new[] {5D, 5, 0},
                                                  new[] {6D, 5, 1},
                                              }
                                          })
                {TestName = "Calculate_WithTwoParameter_ReturnTwoValue"};
        }
    }
}
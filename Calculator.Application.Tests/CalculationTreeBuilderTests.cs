﻿using System;
using System.Collections.Generic;
using NUnit.Framework;

namespace Calculator.Application.Tests
{
    [TestFixture]
    public class CalculationTreeBuilderTests
    {
        private static readonly IDictionary<string, double> sequences = new Dictionary<string, double>
        {
            {"x", 10D},
            {"y", 10D},
            {"z", 10D}
        };

        [Test]
        public void Build_ExpressionIsNull_ThrowArgumentNullException()
        {
            Assert.Throws<ArgumentNullException>(() => CalculationTreeBuilder.Build(null));
        }

        [TestCase("1+1-1", 1, TestName = "Build_SeveralOnePriorityOperation_ReturnCalculation")]
        [TestCase("3*1/2", 1.5, TestName = "Build_SeveralOnePriorityOperation_ReturnCalculation")]
        [TestCase("2-2*2", -2, TestName = "Build_SeveralDifferentPriorityOperation_ReturnCalculation")]
        [TestCase("2*2+2", 6, TestName = "Build_SeveralDifferentPriorityOperation_ReturnCalculation")]
        [TestCase("2^2*3-40", -28, TestName = "Build_SeveralDifferentPriorityOperation_ReturnCalculation")]
        [TestCase("2*2+1-3^2+4%2", -4, TestName = "Build_SeveralDifferentPriorityOperation_ReturnCalculation")]
        public void Build_SeveralOperation(string expression, double expected)
        {
            var tree = CalculationTreeBuilder.Build(expression);

            var actual = tree.Invoke(sequences);

            Assert.That(Math.Abs(expected - actual), Is.LessThan(0.1), () => $"Expected: {expected} But was: {actual}\n");
        }

        [TestCase("1", 1, TestName = "Build_ExpressionIsSimpleNumber_ReturnTreeThatCalculateSimpleNumber")]
        [TestCase("1+1", 2, TestName = "Build_ExpressionWithPlusConst_ReturnSumCalculation")]
        [TestCase("1-1", 0, TestName = "Build_ExpressionWithMinusConst_ReturnCalculation")]
        [TestCase("2^2", 4, TestName = "Build_ExpressionWithPowConst_ReturnCalculation")]
        [TestCase("2*3", 6, TestName = "Build_ExpressionWithMultiplyConst_ReturnCalculation")]
        [TestCase("3/2", 1.5, TestName = "Build_ExpressionWithDivideConst_ReturnCalculation")]
        [TestCase("5%2", 1, TestName = "Build_ExpressionWithModConst_ReturnCalculation")]
        public void Build_Valid(string expression, double expected)
        {
            var tree = CalculationTreeBuilder.Build(expression);

            var actual = tree.Invoke(sequences);

            Assert.That(Math.Abs(expected - actual), Is.LessThan(0.1), () => $"Expected: {expected} But was: {actual}\n");
        }

        [TestCase("2*(3-1)", 4, TestName = "Build_BracketsReorderOperationSequence_ReturnCalculation")]
        [TestCase("2*(2+2)/2", 4, TestName = "Build_BracketsInMiddlePosition_ReturnCalculation")]
        [TestCase("2*((4+3)^2-1)", 96, TestName = "Build_BracketsInsideBrackets_ReturnCalculation")]
        [TestCase("(1+2)/(3-2)", 3, TestName = "Build_TwoBracketsSection_ReturnCalculation")]
        public void Build_WithBrackets(string expression, double expected)
        {
            var tree = CalculationTreeBuilder.Build(expression);

            var actual = tree.Invoke(sequences);

            Assert.That(Math.Abs(expected - actual), Is.LessThan(0.1), () => $"Expected: {expected} But was: {actual}\n");
        }

        [TestCase("sin(0)", 0, TestName = "Build_BracketsWithSinModifier_ReturnCalculation")]
        [TestCase("cos(3,14)", -1, TestName = "Build_BracketsWithCosModifier_ReturnCalculation")]
        [TestCase("sqrt(4)", 2, TestName = "Build_BracketsWithSqrtModifier_ReturnCalculation")]
        [TestCase("trpt(8)", 2, TestName = "Build_BracketsWithTrptModifier_ReturnCalculation")]
        [TestCase("round(4,7)", 5, TestName = "Build_BracketsWithRoundModifier_ReturnCalculation")]
        [TestCase("floor(4,7)", 4, TestName = "Build_BracketsWithFloorModifier_ReturnCalculation")]
        [TestCase("1+floor(4,7)", 5, TestName = "Build_BracketsWithFloorWithPlusModifier_ReturnCalculation")]
        public void Build_WithModifier(string expression, double expected)
        {
            var tree = CalculationTreeBuilder.Build(expression);

            var actual = tree.Invoke(sequences);

            Assert.That(Math.Abs(expected - actual), Is.LessThan(0.1), () => $"Expected: {expected} But was: {actual}\n");
        }

        [TestCase("x", 10, TestName = "Build_OneSequence_ReturnCalculation")]
        [TestCase("x+y", 20, TestName = "Build_TwoSequenceWithSumOperation_ReturnCalculation")]
        public void Build_WithSequence(string expression, double expected)
        {
            var tree = CalculationTreeBuilder.Build(expression);

            var actual = tree.Invoke(sequences);

            Assert.That(Math.Abs(expected - actual), Is.LessThan(0.1), () => $"Expected: {expected} But was: {actual}\n");
        }
    }
}
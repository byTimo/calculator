﻿using System;
using System.Collections.Generic;
using Calculator.Application.Sequences;
using NUnit.Framework;

namespace Calculator.Application.Tests
{
    [TestFixture]
    public class SequenceBuilderTests
    {
        [TestCaseSource(nameof(Build_Cases))]
        public void Build(double[][] input, double[][] expected)
        {
            var actual = SequenceBuilder.Build(input);

            Assert.That(actual, Is.EqualTo(expected));
        }

        private static IEnumerable<TestCaseData> Build_Cases()
        {
            yield return new TestCaseData(Array.Empty<double[]>(), Array.Empty<double[]>())
                { TestName = "Build_EmptyVariables_ReturnEmptyRange"};
            
            yield return new TestCaseData(new[] {new[] {1D}}, new[] {new[] {1D}})
                {TestName = "Build_InputOneVariableWithOneValue_ReturnOneSequenceWithoutValueReapet"};

            yield return new TestCaseData(new[] {new[] {1D, 2, 3}}, new[] {new[] {1D, 2, 3}})
                {TestName = "Build_InputOneVariableWithSeveralValue_ReturnOneSequenceWithoutValueReapet"};

            yield return new TestCaseData(new[]
                                          {
                                              new[] {1D},
                                              new[] {1D}
                                          },
                                          new[]
                                          {
                                              new[] {1D},
                                              new[] {1D}
                                          })
                {TestName = "Build_InputTwoVariableWithOneValue_ReturnOneSequenceWithoutValueReapet"};

            yield return new TestCaseData(new[]
                                          {
                                              new[] {1D},
                                              new[] {1D, 2}
                                          },
                                          new[]
                                          {
                                              new[] {1D, 1},
                                              new[] {1D, 2}
                                          })
                {TestName = "Build_InputSecondVariableWithTwoValue_ReturnReapetValueForFirstVariable"};

            yield return new TestCaseData(new[]
                                          {
                                              new[] {1D, 2},
                                              new[] {1D, 2}
                                          },
                                          new[]
                                          {
                                              new[] {1D, 1, 2, 2},
                                              new[] {1D, 2, 1, 2}
                                          })
                {TestName = "Build_InputBothVariableWithTwoValue_ReturnReapetValueForFirstVariableAndReapetSequenceForSecond"};

            yield return new TestCaseData(new[]
                                          {
                                              new[] {1D, 2},
                                              new[] {10D, 15},
                                              new[] {4D, 5, 6, 7}
                                          },
                                          new[]
                                          {
                                              new[] {1D, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2},
                                              new[] {10D, 10, 10, 10, 15, 15, 15, 15, 10, 10, 10, 10, 15, 15, 15, 15},
                                              new[] {4D, 5, 6, 7, 4, 5, 6, 7, 4, 5, 6, 7, 4, 5, 6, 7}
                                          })
                {TestName = "Build_InputThreeVariableWithSeveralValue_ReturnValidSequence"};
        }
    }
}
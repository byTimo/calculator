﻿using System;
using System.Collections.Generic;
using Calculator.Application;

namespace Сalculator.ConsoleTool
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            var parameters = new List<Parameter>();
            var expression = Console.ReadLine();
            while (true)
            {
                var input = Console.ReadLine();
                if (input == "go")
                {
                    break;
                }
                var strings = input.Split(';');
                parameters.Add(new Parameter(strings[0], double.Parse(strings[1]), double.Parse(strings[2]), double.Parse(strings[3])));
            }

            var calculate = Calculator.Application.Calculator.CalculateAsync(expression, parameters.ToArray()).Result;
            Console.WriteLine(string.Join(",", calculate));
        }
    }
}